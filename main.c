#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <memory.h>

#define PAGE_SIZE 512
#define RECORDS_PER_PAGE 4
#define LINKS_PER_PAGE 3

#define FILE_HEADER_SIZE (2*sizeof(int)) // d, count
#define DIRECTORY_HEADER_SIZE (2*sizeof(int)) // d, count

struct vector {
    int id;
    int x, y;
};

unsigned int full_hash(const unsigned int id) {
    unsigned int hash = id;
    for (int i = 0; i < 30; i++) {
        hash = hash * 1000007 + 21;
    }
    return hash;
}

unsigned int hash(const unsigned int id, const unsigned int d) {
    if (d) {
        return full_hash(id) >> ((8 * sizeof(unsigned int)) - d);
    } else {
        return 0;
    }
}

struct buffered_file {
    FILE *file;
    int buffer_page;
    uint8_t buffer[PAGE_SIZE];
    bool buffer_edited;
} buffered_directory, buffered_file;

int directory_d;
int file_count;

void set_direct_max_offset(const uint8_t *buffer, const int count) {
    ((int *) buffer)[1] = count;
}

void set_direct_d(const uint8_t *buffer, const int d) {
    ((int *) buffer)[0] = d;
}

void set_directory_d(const int d) {
    set_direct_d(buffered_directory.buffer, d);
    buffered_directory.buffer_edited = true;
}

int get_directory_d() {
    return ((int *) buffered_directory.buffer)[0];
}

void set_file_page_d(const int d) {
    set_direct_d(buffered_file.buffer, d);
    buffered_file.buffer_edited = true;
}

int get_file_page_d() {
    return ((int *) buffered_file.buffer)[0];
}

void set_directory_count(const int directory_count) {
    set_direct_max_offset(buffered_directory.buffer, directory_count);
    buffered_directory.buffer_edited = true;
}

int get_directory_count() {
    return ((int *) buffered_directory.buffer)[1];
}

void set_file_max_offset(const int file_page_max_offset) {
    set_direct_max_offset(buffered_file.buffer, file_page_max_offset);
    buffered_file.buffer_edited = true;
}

int get_file_max_offset() {
    return ((int *) buffered_file.buffer)[1];
}

int write_counter = 0;

void write_page(const struct buffered_file *file) {
    assert(file->buffer_page >= 0);

    fseek(file->file, PAGE_SIZE * file->buffer_page, SEEK_SET);
    size_t s = fwrite(file->buffer, PAGE_SIZE, 1, file->file);

    assert(s == 1);

    write_counter++;
}

void write_page_if_edited(struct buffered_file *file) {
    if (file->buffer_edited) {
        write_page(file);
        file->buffer_edited = false;
    }
}

int read_counter = 0;

bool read_page(struct buffered_file *file, const int page) {
    if (file->buffer_page != page) {
        write_page_if_edited(file);
    } else {
        return true;
    }

    int status = fseek(file->file, PAGE_SIZE * page, SEEK_SET);
    assert(status == 0);

    size_t s = fread(file->buffer, PAGE_SIZE, 1, file->file);

    file->buffer_page = page;

    if (s != 1) {
        memset(file->buffer, 0, PAGE_SIZE);
        return false;
    }

    read_counter++;
    return true;
}

void init_file(struct buffered_file *file, const char *file_name) {
    file->file = fopen(file_name, "wb+");
    assert(file->file != NULL);

    file->buffer_page = -1;
    file->buffer_edited = false;
}

void init_directory() {
    bool first_page_exists = read_page(&buffered_directory, 0);
    if (first_page_exists) {
        directory_d = get_directory_d();
        assert(directory_d >= 0);
        file_count = get_directory_count();
        assert(file_count >= 0);
    } else {
        directory_d = 0;
        set_directory_d(directory_d);
        file_count = 1;
        set_directory_count(file_count);
    }
}

void init_records_file(struct buffered_file *file) {
    bool first_page_exists = read_page(file, 0);
    if (!first_page_exists) {
        file->buffer_edited = true;
    }
}

void init_files() {
    init_file(&buffered_directory, "directory.dat");
    init_file(&buffered_file, "file.dat");

    init_directory();
    init_records_file(&buffered_file);
}

void close_file(struct buffered_file *file) {
    write_page_if_edited(file);

    int s = fclose(file->file);
    assert(s == 0);
}

void close_files() {
    close_file(&buffered_directory);
    close_file(&buffered_file);
}

int min(const int a, const int b) {
    return (a < b ? a : b);
}

int get_direct_directory_value(const uint8_t *buffer, const int directory_page_offset) {
    return ((int *) (buffer + DIRECTORY_HEADER_SIZE))[directory_page_offset];
}

int get_directory_value(const int directory_index) {
    const int directory_page = directory_index / LINKS_PER_PAGE;
    const int directory_page_offset = directory_index % LINKS_PER_PAGE;
    bool success = read_page(&buffered_directory, directory_page);
    assert(success);

    return get_direct_directory_value(buffered_directory.buffer, directory_page_offset);
}

void set_directory_value(const int directory_index, const int value) {
    const int directory_page = directory_index / LINKS_PER_PAGE;
    const int directory_page_offset = directory_index % LINKS_PER_PAGE;
    read_page(&buffered_directory, directory_page);

    ((int *) (buffered_directory.buffer + DIRECTORY_HEADER_SIZE))[directory_page_offset] = value;
    buffered_directory.buffer_edited = true;
}

struct vector get_file_value(const int file_page_offset) {
    return ((struct vector *) (buffered_file.buffer + FILE_HEADER_SIZE))[file_page_offset];
}

void set_direct_file_value(const uint8_t *buffer, const int file_page_offset, const struct vector data) {
    ((struct vector *) (buffer + FILE_HEADER_SIZE))[file_page_offset] = data;
}

void set_file_value(const int file_page_offset, const struct vector data) {
    set_direct_file_value(buffered_file.buffer, file_page_offset, data);
    buffered_file.buffer_edited = true;
}

int get_file_page(const int id) {
    const int directory_index = hash(id, directory_d);

    const int file_page = get_directory_value(directory_index);
    assert(file_page >= 0);

    return file_page;
}

struct search_vector_response {
    int file_page;
    int file_page_offset;
    struct vector data;
    bool found;
};

struct search_vector_response search(const int id) {
    struct search_vector_response response;

    response.file_page = get_file_page(id);
    assert(response.file_page >= 0);

    bool success = read_page(&buffered_file, response.file_page);
    assert(success);

    const int file_page_max_offset = get_file_max_offset();
    assert(file_page_max_offset >= 0);

    for (int file_page_offset = 0; file_page_offset < file_page_max_offset; file_page_offset++) {
        if (get_file_value(file_page_offset).id == id) {
            response.data = get_file_value(file_page_offset);
            response.found = true;
            response.file_page_offset = file_page_offset;
            return response;
        }
    }
    response.found = false;
    return response;
}

bool update(const struct vector *data) {
    const struct search_vector_response response = search(data->id);
    if (!response.found) {
        return false;
    }

    set_file_value(response.file_page_offset, *data);
    return true;
}

bool delete(const int id) {
    const struct search_vector_response response = search(id);
    if (!response.found) {
        return false;
    }

    const int file_page_max_offset = get_file_max_offset();
    assert(file_page_max_offset > 0);

    set_file_value(response.file_page_offset, get_file_value(file_page_max_offset - 1));
    set_file_max_offset(get_file_max_offset() - 1);
    return true;
}

bool insert(const struct vector *new_record) {
    const struct search_vector_response response = search(new_record->id);
    if (response.found) {
        return false;
    }

    const int file_page = response.file_page;

    const int file_page_max_offset = get_file_max_offset();
    assert(file_page_max_offset >= 0);
    if (file_page_max_offset >= RECORDS_PER_PAGE) {
        assert(file_page_max_offset == RECORDS_PER_PAGE);

        int file_page_d = get_file_page_d();

        while (true) {
            file_page_d++;

            if (file_page_d > 26) {
                printf("Bad hash function!!!\n");
                assert(false);
            }

            printf("Increasing d for page; new value: %d\n", file_page_d);

            const unsigned int new_record_hash = hash(new_record->id, file_page_d);

            bool found_different_hash = false;

            for (int file_page_offset = 0; file_page_offset < file_page_max_offset; file_page_offset++) {
                const struct vector existing_data = get_file_value(file_page_offset);

                if (hash(existing_data.id, file_page_d) != new_record_hash) {
                    found_different_hash = true;
                    break;
                }
            }

            uint8_t page_1[PAGE_SIZE];
            memset(page_1, 0, PAGE_SIZE);
            int page_1_max_offset = 0;

            if (found_different_hash) {
                int page_2_max_offset = 0;
                uint8_t page_2[PAGE_SIZE];
                memset(page_2, 0, PAGE_SIZE);

                for (int file_page_offset = 0; file_page_offset < file_page_max_offset; file_page_offset++) {
                    const struct vector old_record = get_file_value(file_page_offset);

                    if (hash(old_record.id, file_page_d) & 1) {
                        set_direct_file_value(page_1, page_1_max_offset++, old_record);
                    } else {
                        set_direct_file_value(page_2, page_2_max_offset++, old_record);
                    }
                }

                if (new_record_hash & 1) {
                    set_direct_file_value(page_1, page_1_max_offset++, *new_record);
                } else {
                    set_direct_file_value(page_2, page_2_max_offset++, *new_record);
                }

                set_direct_d(page_2, file_page_d);
                set_direct_max_offset(page_2, page_2_max_offset);

                memcpy(buffered_file.buffer, page_2, PAGE_SIZE);
            } else {
                set_file_page_d(file_page_d);
            }
            buffered_file.buffer_edited = true;
            write_page(&buffered_file);

            set_direct_d(page_1, file_page_d);
            set_direct_max_offset(page_1, page_1_max_offset);

            const int new_file_page = file_count++;

            memcpy(buffered_file.buffer, page_1, PAGE_SIZE);
            buffered_file.buffer_edited = true;
            buffered_file.buffer_page = new_file_page;

            if (file_page_d > directory_d) {
                printf("Increasing d for directory; new value: %d\n", file_page_d);

                const int directory_max_index = 1 << directory_d;
                const int directory_count = (directory_max_index - 1) / LINKS_PER_PAGE;

                uint8_t old_data_buffer[PAGE_SIZE];

                for (int directory_page_with_old_data = directory_count;
                     directory_page_with_old_data >= 0; directory_page_with_old_data--) {
                    bool success = read_page(&buffered_directory, directory_page_with_old_data);
                    assert(success);

                    memcpy(old_data_buffer, buffered_directory.buffer, PAGE_SIZE);

                    const int directory_page_max_index =
                            min(directory_max_index, LINKS_PER_PAGE * (directory_page_with_old_data + 1)) - 1;
                    for (int directory_index_with_old_data = directory_page_max_index; directory_index_with_old_data >=
                                                                                       (directory_page_max_index /
                                                                                        LINKS_PER_PAGE) *
                                                                                       LINKS_PER_PAGE; directory_index_with_old_data--) {
                        const int directory_page_offset_with_old_data = directory_index_with_old_data % LINKS_PER_PAGE;
                        const int file_page_with_old_data = get_direct_directory_value(old_data_buffer,
                                                                                       directory_page_offset_with_old_data);

                        for (int offset = 1; offset >= 0; offset--) {
                            int directory_index = directory_index_with_old_data * 2 + offset;

                            if (file_page_with_old_data == file_page && (
                                    (found_different_hash && offset) ||
                                    (!found_different_hash && offset != (new_record_hash & 1)))) {
                                set_directory_value(directory_index, new_file_page);
                            } else {
                                set_directory_value(directory_index, file_page_with_old_data);
                            }
                        }
                    }
                }

                assert(buffered_directory.buffer_page == 0);

                directory_d = file_page_d;
                set_directory_d(directory_d);
            } else {
                const int d_diff = directory_d - file_page_d;

                const int count = 1 << d_diff;
                unsigned int directory_base_index = (new_record_hash & (~1)) << d_diff;
                if (found_different_hash || !(new_record_hash & 1)) {
                    directory_base_index += count;
                }
                for (int offset = 0; offset < count; offset++) {
                    int directory_index = directory_base_index + offset;

                    set_directory_value(directory_index, new_file_page);
                }

            }
            if (found_different_hash) {
                break;
            }

            bool success = read_page(&buffered_file, file_page);
            assert(success);
        }

        bool first_page_exists = read_page(&buffered_directory, 0);
        assert(first_page_exists);

        set_directory_count(file_count);
    } else {
        set_file_value(file_page_max_offset, *new_record);
        set_file_max_offset(get_file_max_offset() + 1);
    }
    return true;
}

void print_bits(const unsigned int a, const int d) {
    if (d) {
        for (int i = 0; i < d; i++) {
            printf("%c", (a & (1 << (d - i - 1))) ? '1' : '0');
        }
    } else {
        printf("_");
    }
}

void print_directory() {
    const int directory_max_index = 1 << directory_d;
    printf("Directory\n");
    printf("\tbits = %d\n", directory_d);
    for (int directory_index = 0; directory_index < directory_max_index; directory_index++) {
        const int file_page = get_directory_value(directory_index);

        printf("\t\t");
        print_bits(directory_index, directory_d);
        printf(" -> %d\n", file_page);
    }
}

void print_file() {
    for (int file_page = 0; file_page < file_count; file_page++) {
        read_page(&buffered_file, file_page);
        const int file_page_max_offset = get_file_max_offset();
        const int file_page_d = get_file_page_d();

        printf("Page %d\n", file_page);
        printf("\tcount = %d\n", file_page_max_offset);
        printf("\tbits = %d\n", file_page_d);

        for (int file_page_offset = 0; file_page_offset < file_page_max_offset; file_page_offset++) {
            const struct vector data = get_file_value(file_page_offset);

            printf("\t\t");
            print_bits(hash(data.id, file_page_d), file_page_d);
            if (directory_d > file_page_d) {
                printf("(");
                print_bits(hash(data.id, directory_d), directory_d - file_page_d);
                printf(")");
            }
            printf(", value %d [%d, %d]\n", data.id, data.x, data.y);
        }
    }
}

void print() {
    print_directory();
    print_file();
    printf("\n");
}

int main() {
    assert(LINKS_PER_PAGE > 0);
    assert(LINKS_PER_PAGE * sizeof(int) + DIRECTORY_HEADER_SIZE <= PAGE_SIZE);
    assert(RECORDS_PER_PAGE > 0);
    assert(RECORDS_PER_PAGE * sizeof(struct vector) + FILE_HEADER_SIZE <= PAGE_SIZE);

    init_files();

    struct vector data;

    {
        int mode = 0;
        int s;

        while (mode < 1 || mode > 2) {
            printf("Podaj tryb:\n\t1 - sekwencja\n\t2 - ręczne operacje\n");
            s = scanf("%d", &mode);
        }

        if (mode == 1) {
            int n;

            printf("Podaj liczbę rekordów n\n");
            s = scanf("%d", &n);
            assert(n > 0);

            for (int i = 0; i < n; i++) {
                data.id = i;
                data.x = 5 * i;
                data.y = i * 10;

                bool success = insert(&data);
                assert(success);
            }

            for (int i = n - 1; i >= 0; i--) {
                data.id = i;
                data.x = 5 * i;
                data.y = i * 10;

                struct search_vector_response response = search(data.id);

                assert(response.found);
                assert(response.data.id == data.id);
                assert(response.data.x == data.x);
                assert(response.data.y == data.y);
            }

            for (int i = 0; i < n; i++) {
                data.id = i;
                data.x = i;
                data.y = i + 5;

                bool success = update(&data);
                assert(success);
            }

            for (int i = n - 1; i >= 0; i--) {
                data.id = i;
                data.x = i;
                data.y = i + 5;

                struct search_vector_response response = search(data.id);

                assert(response.found);
                assert(response.data.id == data.id);
                assert(response.data.x == data.x);
                assert(response.data.y == data.y);
            }

            for (int i = n - 1; i >= 0; i--) {
                data.id = i;

                bool success = delete(data.id);
                assert(success);
            }

            for (int i = n - 1; i >= 0; i--) {
                data.id = i;

                struct search_vector_response response = search(data.id);
                assert(!response.found);
            }
        } else {
            bool exit = false;

            while (!exit) {
                int op;
                printf("Podaj rodzaj operacji:\n\t1 - dodawanie (key, x, y)\n\t2 - modyfikacja (key, x, y)\n\t3 - usuwanie (key)\n\t4 - wyświetlanie struktury ()\n\t5 - wyszukiwanie rekordu (key)\n\t6 - wyjście\n");
                s = scanf("%d", &op);

                int read_tmp = read_counter;
                int write_tmp = write_counter;

                switch (op) {
                    case 1: {
                        s = scanf("%d%d%d", &data.id, &data.x, &data.y);
                        if (s == 3) {
                            bool success = insert(&data);
                            printf("%s\n", success ? "Ok" : "Id collision");
                        } else {
                            printf("Bad arguments\n");
                        }
                    }
                        break;
                    case 2: {
                        s = scanf("%d%d%d", &data.id, &data.x, &data.y);
                        if (s == 3) {
                            bool success = update(&data);
                            printf("%s\n", success ? "Ok" : "Not found");
                        } else {
                            printf("Bad arguments\n");
                        }
                    }
                        break;
                    case 3: {
                        s = scanf("%d", &data.id);
                        if (s == 1) {
                            bool success = delete(data.id);
                            printf("%s\n", success ? "Ok" : "Not found");
                        } else {
                            printf("Bad arguments\n");
                        }
                    }
                        break;
                    case 4: {
                        print();
                    }
                        break;
                    case 5: {
                        s = scanf("%d", &data.id);
                        if (s == 1) {
                            struct search_vector_response response = search(data.id);
                            if (response.found) {
                                printf("Found %d [%d, %d]\n", response.data.id, response.data.x, response.data.y);
                            } else {
                                printf("Not found\n");
                            }
                        } else {
                            printf("Bad arguments\n");
                        }
                    }
                        break;
                    case 6: {
                        exit = true;
                    }
                        break;
                    default: {
                        printf("Bad operation\n");
                    }
                }

                printf("Statistics: reads %d, writes %d\n", read_counter - read_tmp, write_counter - write_tmp);
            }
        }
    }

    close_files();
    return 0;
}
