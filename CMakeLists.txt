cmake_minimum_required(VERSION 3.17)
project(sbd_hash C)

set(CMAKE_C_STANDARD 11)

add_executable(sbd_hash main.c)
add_definitions(-Wall)